import gdb

class InspectPointCommand(gdb.Command):
    """Inspect the specified point in the BoardState."""

    def __init__(self):
        super(InspectPointCommand, self).__init__("inspect_point", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)

        if len(args) != 2:
            print("Usage: inspect_point [object] [index]")
            return

        object_expr = args[0]
        index_expr = args[1]

        try:
            obj = gdb.parse_and_eval(object_expr)
            index = int(gdb.parse_and_eval(index_expr))

            if str(obj.type) != "BoardState":
                print("Error: The specified object is not of type BoardState.")
                return

            points_vector = obj['m_points']

            #print("ok1")
            if index < 0 or index >= gdb.parse_and_eval(f"{object_expr}.m_points.size()"):
                print(f"Error: Invalid index {index}.")
                return
            #print("ok2")

            point = gdb.parse_and_eval(f"{object_expr}.m_points[{index}]")

            #print("ok3")
            print(f"Point {index}:")
            print(f"  Count: {point['m_count']}")
            owner = point['m_owner']
            if owner:
                print(f"  Owner: {owner}")
            else:
                print("  Owner: None")

        except gdb.error as e:
            print(f"Error: {e}")

InspectPointCommand()
