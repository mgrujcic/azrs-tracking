FROM ubuntu:latest

COPY BackgammonPP /workdir/BackgammonPP

WORKDIR /workdir

RUN apt-get update -y && \ 
	apt-get install -y git cmake ninja-build g++ qt6-base-dev qt6-multimedia-dev libglx-dev libgl1-mesa-dev

RUN cmake -G Ninja -B build/ -S BackgammonPP/ && ninja -C build/

WORKDIR /workdir/build

ENTRYPOINT ["./BackgammonPP"]



