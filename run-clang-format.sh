#!/bin/sh

lines=$(find BackgammonPP -type f \( -name "*.h" -o -name "*-hpp" -o -name "*cpp" \) -printf "%p\n")

for line in $lines; do
	echo $line
	clang-format $line -i 
done
